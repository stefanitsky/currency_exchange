from rest_framework import serializers

from currency_exchange.users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email", "name",)
